const { srcDir, rootDir } = require('./utils');
const fs = require('fs');
const modernBrowsers = fs.readFileSync( rootDir + '/.plop/browserslist/modern.js.hbs', 'utf8');
const legacyBrowsers = fs.readFileSync( rootDir + '/.plop/browserslist/legacy.js.hbs', 'utf8');
const ancientBrowsers = fs.readFileSync( rootDir + '/.plop/browserslist/ancient.js.hbs', 'utf8');

module.exports = function (plop) {
		// create your generators here
		plop.setGenerator('setup', {
				description: 'this is a skeleton plopfile',
				prompts: [
					{
						type: 'input',
						name: 'projectName',
						message: 'What is the name of the project ?',
						validate(inputValue) {
							let packageNameTest = /^(?:@[a-z0-9-~][a-z0-9-._~]*\/)?[a-z0-9-~][a-z0-9-._~]*$/;
							let valid = packageNameTest.test(inputValue);

							return (
								!!valid ||
								'The name of the project must match the pattern. See https://docs.npmjs.com/files/package.json for rules.'
							);
						},
					},
					{
						type: 'list',
						name: 'bootstrapVersion',
						message: 'Which version of Twitter Bootstrap will project be using?',
						choices: [
							{
								name: `~3.4.0 (Legacy)`,
								value: 'bootstrap3',
							},
							{
								name: `~4.4.0 (Modern)`,
								value: 'bootstrap',
							},
						],
					},
					{
						type: 'list',
						name: 'projectType',
						message: 'What type of project are you creating ?',
						choices: [
							{
								name: 'Static HTML with jQuery',
								value: 'jquery',
							},
							{
								name: 'HTML components for Angular, React.js',
								value: false,
							},
							{
								name: 'Vue.js',
								value: 'vue',
							},
							{
								name: 'Liferay Theme',
								value: 'liferay',
							},
						],
					},
					{
						type: 'list',
						name: 'supportedBrowsers',
						message: 'Does the project need to support other browsers than specified ?',
						choices: [
							{
								name: `No (${modernBrowsers.replace(/(\r\n|\n|\r)/gm,', ')})`,
								value: 'modern',
							},
							{
								name: `Old (${legacyBrowsers.replace(/(\r\n|\n|\r)/gm,', ')})`,
								value: 'legacy',
							},
							{
								name: `Ancient (${ancientBrowsers.replace(/(\r\n|\n|\r)/gm,', ')})`,
								value: 'ancient',
							},
						],
					},
				],
				actions: ({ projectType, bootstrapVersion, supportedBrowsers }) => {
					/* TODO: Maybe choice between specific versions ? */
					const bootstrapSemver = bootstrapVersion === 'bootstrap3' ? '"bootstrap-sass": "~3.4.0"' : '"bootstrap": "~4.4.0"';
					const liferay = projectType === 'liferay' ? { liferay: true } : { liferay: false };

					return [
						{
							type: 'modify',
							path: rootDir + '/package.json',
							pattern: /\"name\"\:\s\"(?:@[a-z0-9-~][a-z0-9-._~]*\/)?[a-z0-9-~][a-z0-9-._~]*\"/gi,
							template: '"name": "{{projectName}}"'
						},
						{
							type: 'add',
							path: rootDir + '/package.json',
							force: true,
							data: {
								jquery: projectType === 'jquery' ? true : false,
								vue: projectType === 'vue' ? true : false,
							},
							templateFile: rootDir + `/.plop/config/package.json.hbs`,
						},
						{
							type: 'modify',
							path: rootDir + '/package.json',
							pattern: /\"bootstrap(-sass)?\"\:\s\"((\^*|\~*)(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)|(latest))\"/gi,
							template: bootstrapSemver
						},
						{
							type: 'add',
							path: srcDir + '/scss/01_vendors/bootstrap_init.scss',
							pattern: /\"bootstrap(-sass)?\"\:\s\"((\^*|\~*)(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)|(latest))\"/gi,
							templateFile: rootDir + `/.plop/bootstrap/{{bootstrapVersion}}_init.scss.hbs`,
							skipIfExists: true
						},
						{
							type: 'add',
							path: srcDir + '/scss/front.scss',
							data: liferay,
							templateFile: rootDir + `/.plop/scss/front.scss.hbs`,
							skipIfExists: true
						},
						{
							type: 'add',
							path: srcDir + '/scss/print.scss',
							data: liferay,
							templateFile: rootDir + `/.plop/scss/print.scss.hbs`,
							skipIfExists: true
						},
						{
							type: 'add',
							path: srcDir + '/scss/01_config/_variables.scss',
							data: liferay,
							templateFile: rootDir + `/.plop/scss/01_config/_variables.scss.hbs`,
							skipIfExists: true
						},
						{
							type: 'add',
							path: rootDir + '/.browserslistrc',
							templateFile: rootDir + `/.plop/browserslist/${supportedBrowsers}.js.hbs`,
							skipIfExists: true
						},
						{
							type: 'add',
							path: srcDir + '/static/pages/index.html',
							templateFile: rootDir + `/.plop/html/index.html.hbs`,
							skipIfExists: true
						},
						...(projectType === 'liferay' ? [
							{
								type: 'add',
								path: srcDir + '/scss/_custom.scss',
								templateFile: rootDir + `/.plop/scss/_custom.scss.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: rootDir + '/webpack/webpack.settings.js',
								templateFile: rootDir + `/.plop/config/webpack.settings.js.hbs`,
								force: true
							},
							{
								type: 'add',
								path: rootDir + '/package.json',
								templateFile: rootDir + `/.plop/config/package-liferay.json.hbs`,
								force: true
							},
							{
								type: 'add',
								path: rootDir + '/gulpfile.js',
								templateFile: rootDir + `/.plop/config/gulpfile.js.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: rootDir + '/liferay-theme.json',
								templateFile: rootDir + `/.plop/config/liferay-theme.json.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: srcDir + '/css/_custom.scss',
								templateFile: rootDir + `/.plop/css/_custom.scss.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: srcDir + '/WEB-INF/liferay-look-and-feel.xml',
								templateFile: rootDir + `/.plop/WEB-INF/liferay-look-and-feel.xml.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: srcDir + '/WEB-INF/liferay-plugin-package.properties',
								templateFile: rootDir + `/.plop/WEB-INF/liferay-plugin-package.properties.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: srcDir + '/scss/front-liferay.scss',
								templateFile: rootDir + `/.plop/scss/front-liferay.scss.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: srcDir + '/scss/_liferay-all.scss',
								templateFile: rootDir + `/.plop/scss/_liferay-all.scss.hbs`,
								skipIfExists: true
							},
							{
								type: 'add',
								path: srcDir + '/scss/01_config/01_variables/_liferay.scss',
								templateFile: rootDir + `/.plop/scss/01_config/01_variables/_liferay.scss.hbs`,
								skipIfExists: true
							},
						] : []),
					]
				}
		});
};
