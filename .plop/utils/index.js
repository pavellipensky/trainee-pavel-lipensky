const path = require('path');

const rootDir = path.resolve(__dirname, '../../');
const srcDir = path.resolve(rootDir, 'src');

module.exports = {
	srcDir,
	rootDir
}