# Coding Starter Pack (CSP)

### Required tools
* [Node] (http://nodejs.org) ^13.x
* [Yarn] (https://yarnpkg.com/getting-started/install) ^1.22.10

### Installation a development
0. Clone project `git clone git@bitbucket.org:lundegaard/coding-starter-pack-webpack.git`
1. Install npm packages with `yarn install`
2. Start config with `yarn setup`
3. Read and carefully choose answers in prompt	
	* If you chose older Bootstrap, run `yarn install` again to install correct packages
	* If you chose Liferay theme, run `yarn install` again to install correct packages
4. Develop (`yarn start`)

### Production builds
1. Install npm packages with `yarn install`
2. Run build with `yarn build`

### Liferay Theme Development
1. Comment out body and html selectors in vendors/bootstrap/boostrap/_scaffolding.scss
2. Do these following steps for more cleaner and maintainable code:
	* If you don't need to use Normalize comment out `@import` "bootstrap/normalize"; in vendors/bootstrap/_bootstrap.scss
	* If you do need to use Normalize styles, comment out only body and html selectors in vendors/bootstrap/bootstrap/_normalize.scss
